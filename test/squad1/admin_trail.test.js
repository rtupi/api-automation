const createUser = require('../../src/data/CreateUserData')
const { adminSchema } = require('../../src/schemas/adminSchema')
const AdminService = require('../../src/service/adminService')

const chai = require('chai')
const assert = chai.assert
chai.use(require('chai-json-schema'))

describe('#Person', () => {
    const adminService = new AdminService()
    let response

    describe('#GET', () => {
        describe('Listagem Completa', () => {

            beforeAll(async() => {
                response = await adminService.getAll()
            })

            it('#status code', async() => {
                assert.equal(response.status, 200)
            })

            it('#contract', async() => {
                assert.jsonSchema(response.body, adminSchema)
            })

            it('#headers', async() => {
                assert.equal(response.header['content-type'], 'application/json; charset=utf-8')
            })

            it('#response body', async() => {

                response.body.forEach(element => {
                    assert(element.id > 0)
                    assert(element.instructor_names)
                    assert(element.objective_titles)
                    assert(element.stage_titles)
                });

            })
        })
    })
})