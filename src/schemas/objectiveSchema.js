const objectiveSchema = {
    type: "array",
    items: {
        type: "string"
    }
}

module.exports = { objectiveSchema }