const adminSchema = {
    type: "array",
    items: {
        type: "object",
        required: ['id', 'cover_url', 'duration', 'instructor_names', 'objective_titles', 'rating', 'stage_titles', 'title'],
        properties: {
            id: {
                "type": "number"
            },
            cover_url: {
                type: "string"
            },
            duration: {
                type: "number"
            },
            instructor_names: {
                type: "array",
                items: {
                    type: "string"
                }
            },
            objective_titles: {
                type: "array",
                items: {
                    type: "string"
                }
            },
            rating: {
                type: "number"
            },
            stage_titles: {
                type: "array",
                items: {
                    type: "string"
                }
            },
            title: {
                type: "string"
            }
        }
    }
}

module.exports = { adminSchema }